package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private Long id;
    private String nome;
    private AreaCientifica areaCientifica;

    public Curso(AreaCientifica areacientifica) {
        this.areaCientifica = areacientifica;
        areacientifica.adicionarCurso(this);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }
}
