package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements Serializable {

    private static final long serialVersionUID = -469976309697117212L;

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacao;
    private Estudante estudante;

    public Inscricao(Seminario seminari) {
        this.seminario = seminari;
        this.seminario.adicionarInscricao(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCriacao = LocalDateTime.now();

    }

    public void comprar(Estudante estudant, Boolean direitomaterial) {
        this.estudante = estudant;
        this.direitoMaterial = direitomaterial;
        this.dataCompra = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
        this.estudante.adicionarInscricao(this);

    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.dataCompra = null;
        this.direitoMaterial = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void realizarCheckIn() {
        this.dataCheckIn = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }
}
