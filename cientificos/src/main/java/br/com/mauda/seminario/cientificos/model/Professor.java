package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements Serializable {

    private static final long serialVersionUID = -2713724483725712786L;

    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private Double salario;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarSeminario(Seminario seminarios) {
        this.seminarios.add(seminarios);
    }

    public boolean possuiSeminario(Seminario seminarios) {
        return this.seminarios.contains(seminarios);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

}
