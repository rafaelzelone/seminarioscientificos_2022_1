package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AreaCientifica implements Serializable {

    private static final long serialVersionUID = -8550653178733527667L;

    private Long id;
    private String nome;
    private List<Curso> cursos = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public AreaCientifica() {
        // TODO Auto-generated constructor stub
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
