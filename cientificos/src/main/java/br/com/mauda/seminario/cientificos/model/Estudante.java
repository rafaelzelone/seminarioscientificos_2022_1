package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Estudante implements Serializable {

    private static final long serialVersionUID = 7419272579329248201L;

    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private List<Inscricao> inscricoes = new ArrayList<>();
    private Instituicao Instituicao;

    public Estudante(Instituicao instituicao) {
        this.Instituicao = instituicao;
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removerInscricao(Inscricao inscricao) {
        this.inscricoes.remove(inscricao);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Instituicao getInstituicao() {
        return this.Instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
